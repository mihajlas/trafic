/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Trafic;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author Mihajlo
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class CSVReader {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        String csvFile = "/Users/trafic.csv";
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

               
                String[] trafic = line.split(cvsSplitBy);

                System.out.println("Trafic [id= " + trafic[0] + " , views=" + trafic[1] + " , clicks=" + trafic[2] + "]");
      int views = Integer.parseInt(trafic[1]);
      String myDriver = "org.gjt.mm.mysql.Driver";
      String myUrl = "jdbc:mysql://localhost/trafic";
      Class.forName(myDriver);
      Connection conn = DriverManager.getConnection(myUrl, "root", "");
    

      Calendar calendar = Calendar.getInstance();
      java.sql.Date viewDate = new java.sql.Date(calendar.getTime().getTime());

      String query = " insert into trafic (Views,Date)"
        + " values (?, ?)";

      PreparedStatement preparedStmt = conn.prepareStatement(query);
      preparedStmt.setInt (1, views);
      preparedStmt.setDate (2, viewDate);
 

     
      preparedStmt.execute();
      
      conn.close();
      Timer timer = new Timer ();
TimerTask hourlyTask = new TimerTask () {
    @Override
    public void run () {
        System.out.println("Database insert every 24 hours");
    }
};

// schedule the task to run starting now and then every 24 hours...
timer.schedule (hourlyTask, 0l, 1000*60*60*24);
      
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
/*
Objasnjenje zadatka:
Koristio sam java File reader da ucitam trazeni csv file(trafic.csv)
extractovao vrednosti iz fajla, napravio variablu views koja nam i 
treba za pracenje posete blogova dobijenu varijablu sam upisao u bazu
i upisao sam vreme.Svaki sledeci entri sledeceg dana predstavlja preglede proslog plus preglede danasnjeg dana,
ovo se moze lako srediti u excelu kad se MySql baza konvertuje u potreban format
koji odgovara analiticaru
/1 Uradjen listener da se radi insert u bazu svaka 24 sata

2a. zadatak
String upit = "SELECT Department family,SUM(AP Amount (£)) from table"
    + "where Department family = "Department of Health";



2b. String upit = "SELECT Department family,SUM(AP Amount (£)), Expense type from table"
    + "where Department family = "Department of Health"
    + "group by Expense type"

3. Creation of pivot table
CREATE TABLE pivot
  AS (

SELECT *
from 
(
  SELECT Expense type, Expense area, AP amount
  from table
) src
pivot
(
  sum(AP amount)
  for Expense type in ([1], [2], [3], [4], [5], [6], [7])
) piv;
)












*/





